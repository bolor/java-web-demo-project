<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
9*9

<table border=1>

<% for(int i=1; i<10; i++) {%>

	<tr>

	<% for(int j=1; j<10; j++) {%>
	
		<td>
			<% if(i>=j) {%>
				<%=i %> * <%= j %> = <%= i*j %>
			<% } %>
		</td>
	
	<%} %>

	</tr>

<%} %>

</table>

</body>
</html>
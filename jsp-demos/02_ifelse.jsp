<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.Date" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>


<% Date now = new Date(); 

int hour = now.getHours();
int minute = now.getMinutes();

%>


<h1 style="color: <%= hour<12? "orange":"blue"%>">当前时间是 <%=hour %>:<%=minute %></h1>

<% if(hour<12) {%>
<h1>上午</h1>
<%} else {%>
<h1>下午</h1>
<% } %>


</body>
</html>